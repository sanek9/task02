task02: main.c
	gcc main.c -lm -o task02

all: task021 task022 task023

task021: main.c
	gcc -D NUMBER_OF_TASK=1 main.c -lm -o task021

task022: main.c
	gcc -D NUMBER_OF_TASK=2 main.c -lm -o task022

task023: main.c
	gcc -D NUMBER_OF_TASK=3 main.c -lm -o task023
clean:
	rm task02* 

