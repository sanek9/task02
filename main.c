#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>

float circleArea(float radius);
float rectangleArea(float length, float width);
int max(int a, int b, int c);

int main(int argc, char** argv[]){
	int x;
#ifndef NUMBER_OF_TASK
	printf("Введите номер задания\n");
	scanf("%d",&x);
	switch (x){
		case 1:
#endif
#if !defined(NUMBER_OF_TASK) || NUMBER_OF_TASK==1
			printf("Площадь окружности\nрадиусом 1=%f\nрадиусом 5=%f\nрадиусом 10=%f\n",
				circleArea(1), circleArea(5),circleArea(10));
			//circleArea(10);
#endif
#ifndef NUMBER_OF_TASK
		break;
		case 2:
#endif
#if !defined(NUMBER_OF_TASK) || NUMBER_OF_TASK==2
			printf("Площади прямоугольников, где\n\
				a=5 b=5 S=%f\na=1 b=10 S=%f\na=3.3 b=2.2 S=%f\n",
				rectangleArea(5,5),rectangleArea(1,10),rectangleArea(3.3,2.2));
#endif
#ifndef NUMBER_OF_TASK
		break;
		case 3:
#endif
#if !defined(NUMBER_OF_TASK) || NUMBER_OF_TASK==3
			printf("Введите 3 числа и будет выведено наибольшее значение\n");
			int A,B,C;
			scanf("%d %d %d",&A,&B,&C);
			printf("Найбольшее число %d из %d %d %d\n", max(A,B,C),A,B,C);
#endif
#ifndef NUMBER_OF_TASK
		break;
		default:
			printf("от 1-3\n");
		break;
	} 
#endif
}
#if !defined(NUMBER_OF_TASK) || NUMBER_OF_TASK==1
float circleArea(float radius){
	return (M_PI*pow(radius,2));
}
#endif
#if !defined(NUMBER_OF_TASK) || NUMBER_OF_TASK==2
float rectangleArea(float length, float width){
	return length*width;
}
#endif
#if !defined(NUMBER_OF_TASK) || NUMBER_OF_TASK==3
int max(int a, int b, int c){
	if (a>b){
		if (a>c){
			return a;
		}else
			return c;
	}else if (b>c){
		return b;
	}else return c;
}
#endif

